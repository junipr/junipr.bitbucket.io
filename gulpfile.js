var gulp = require('gulp');
var sass = require('gulp-sass');
var bourbon = require('node-bourbon');
var neat = require('node-neat');
let cleancss = require('gulp-clean-css');
var minify = require('gulp-minify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var merge2 = require('merge2')
var jsvalidate = require('gulp-jsvalidate');

gulp.task('sass', function(){
  return gulp.src('assets/css/src/main.scss')
    .pipe(sass({
      includePaths: [].concat(bourbon.includePaths,neat.includePaths)
    }))
    .pipe(cleancss({debug: true}, (file) => {
      console.log('- CSS Minified -');
      console.log(`Original Size: ${file.stats.originalSize}`);
      console.log(`Minified Size: ${file.stats.minifiedSize}`);
    }))
    .pipe(rename('bundle.css'))
    .pipe(gulp.dest('assets/css/dist')
  )
});

gulp.task('default', function() {
    gulp.watch('assets/css/src/*.scss', ['sass'])
});
